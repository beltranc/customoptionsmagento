<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

$mageFilename = '/app/Mage.php';
require_once $mageFilename;
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
umask(0);
Mage::app('admin');
Mage::register('isSecureArea', 1);

$orderIncrementId = '100000179'; 
$order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

foreach ($order->getAllItems() as $item){
    if(($item->getProduct()->getTypeID() == 'configurable')){
        $items = $item->getProductOptions();
		$options = $items['options'];
		foreach ($options as $option) {
			$optionTitle = $option['label'];
			$optionId = $option['option_id'];
			$optionValue = $option['value'];
			
			echo $optionTitle.' - '.$optionValue.'<br>';
        }
    }
}