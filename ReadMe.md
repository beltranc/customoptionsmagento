Code snippet to get custom options value of all products in any Magento order.

Suppose, there is a product with a custom option dropdown list. You chose one of the option from the dropdown list and purchased that product. The code below fetches product’s custom option value selected in any particular order.